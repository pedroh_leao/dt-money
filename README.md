# DT-Money
<p align="justify">Projeto criado durante o curso de ReactJS no qual foi desenvolvido um software de controle financeiro onde é possível: <br>
    <ul>
        <li>criar novas transações financeiras de entrada e saída</li>
        <li>ver uma lista das transações com o título, valor, categoria e data das mesmas </li>
        <li>ver o valor final das entradas, saídas e o total</li>
    <ul>
</p>
